package com.magic.magicstorecard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MagicStoreCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicStoreCardApplication.class, args);
    }
}
