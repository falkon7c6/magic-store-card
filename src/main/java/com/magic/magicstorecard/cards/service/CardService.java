package com.magic.magicstorecard.cards.service;

import com.magic.magicstorecard.cards.model.Card;
import com.magic.magicstorecard.cards.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardService {

    private final CardRepository cardRepository;

    public Card findById(Long id) {
        log.info(String.format("looking for card with id: %s", id));
        return cardRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("The card with id %s was not found.", id)));
    }

    public List<Card> findAll() {
        log.info("retrieving all cards.");
        return cardRepository.findAll();
    }

    public Card createCard(Card card) {
        log.info(String.format("saving card:  %s", card));
        return cardRepository.save(card);
    }

    public Card updateCard(Card card) {
        log.info(String.format("updating card:  %s", card));
        return cardRepository.save(card);
    }

    public void deleteCard(Card card) {
        log.info(String.format("deleting card:  %s", card));
        cardRepository.delete(card);
    }
}
