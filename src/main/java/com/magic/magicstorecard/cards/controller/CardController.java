package com.magic.magicstorecard.cards.controller;

import com.magic.magicstorecard.cards.model.Card;
import com.magic.magicstorecard.cards.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getCard(@PathVariable("id") Long id) {
        return ResponseEntity.ok(cardService.findById(id));
    }

    @GetMapping
    public ResponseEntity<?> getAllCards() {
        return ResponseEntity.ok(cardService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> saveCard(@RequestBody Card card) {
        Card newCard = cardService.createCard(card);
        URI location = URI.create(String.format("/card/%s", newCard.getId()));
        return ResponseEntity.created(location).body(newCard);
    }

    @PutMapping()
    public ResponseEntity<?> updateCard(@RequestBody Card card) {
        cardService.updateCard(card);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteCard(@RequestBody Card card) {
        cardService.deleteCard(card);
        return ResponseEntity.noContent().build();
    }
}
